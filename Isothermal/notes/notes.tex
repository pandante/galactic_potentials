\documentclass[10pt, a4paper]{article}

\usepackage{natbib}
\usepackage{graphicx,color}
\usepackage{amsmath,amssymb}
\usepackage{wasysym}

% =======================
% commands in math mode
% =======================

% --- journals ---
\newcommand{\apj}{ApJ}

% --- variables ---
\newcommand{\ud}{\mathrm{d}}

% --- units ---
\newcommand{\yr}{\,\mathrm{y}}
\newcommand{\Msun}{\,\mathrm{M}_{\odot}} % need mathabx
\newcommand{\kpc}{\,\mathrm{kpc}}
\newcommand{\Myr}{\,\mathrm{My}}


\title{Quasi-selfsimilar collapse of an isothermal sphere}
\author{AYW}
\date{\today}

\begin{document}

\maketitle
\section{Introduction}
The following outlines the basic physics of isothermal spheres of baryonic gas, in particular the case of an initially singular, hydrostatically unstable sphere that undergoes collapse in a self-similar fashion. The notes reproduce the study by \citet{Shu1977}.

Section~\ref{sec:hesis} derives the dimensional and dimensionless forms of the Lane-Emden equation and comments on the conditions for stability of a solution. In Sect.~\ref{sec:sscis} the relevant conservation equations for spherically symmetric isothermal collapse are written down, and quasi-selfsimilar solutions are shown. Section~\ref{sec:disc} brings up some points of discussion.

\section{Hydrostatic equilibrium of a self-gravitating isothermal sphere}\label{sec:hesis}
We first write down the relations between density, pressure and gravitational potential for a self-gravitating isothermal sphere. Poisson's equation for these conditions is the Lane-Emden equation, the solution to which are Bonnor-Ebert spheres. A special case of the Bonnor-Ebert spheres are singular isothermal spheres. We note the conditions for stable and unstable hydrostatic equilibrium of (bounded) Bonnor-Ebert and singular isothermal spheres. First, the equations are given in dimensional form, then non-dimensional variables (based on a similarity variable) are introduced, with which the equations are expressed in non-dimensional form.

\subsection{Equations in dimensional form}
The condition for hydrostatic equilibrium is
\begin{equation}
  \nabla{}P=-\rho\nabla\phi\,.\label{eq:hyeq}
\end{equation}
$\phi$ is the gravitational potential, $P$ and $\rho$ are the gas pressure and density, respectively. In spherical coordinates, using the isothermal equation of state $P=a^2\rho$, where $a$ is the isothermal sound speed, Eqn.~\eqref{eq:hyeq} reads
\begin{equation}
  \frac{a^2}{\rho}\frac{\ud \rho}{\ud r}=-\frac{\ud\phi}{\ud r}\,,\label{eq:hyeqr}
\end{equation}
and has the solution
\begin{equation}
  \rho=\rho_0e^{\frac{-\phi(r)}{a^2}}\,.\label{eq:hyeqsol}
\end{equation}
This relation between $\phi(r)$ and $\rho(r)$ describes the condition for hydrostatic equilibrium at any $r$, where $r$ is the radial spatial coordinate, and $\rho_0$ is the central value of the density.

Poisson's equation gives the relationship between $\phi(r)$ and $\rho(r)$ under self-gravity. Combining with Eqn.~\eqref{eq:hyeqsol} we obtain the Lane-Emden equation for an isothermal sphere 
\begin{equation}
  \frac{1}{r^2}\frac{\ud}{\ud r}\left(r^2\phi(r)\right)=4\pi{}G\rho=4\pi{}G\rho_0e^{\frac{-\phi(r)}{a^2}}\,.\label{eq:laneemden}
\end{equation}
The solutions of Eqn.~\eqref{eq:laneemden} for $\ud\phi(0)/\ud r=0$ and $\phi(0)=0$ are called Bonner-Ebert spheres. An unbounded Bonner-Ebert sphere approaches $\rho\rightarrow{}a^2/2\pi{}Gr^2$ at large $r$ and in the limit of infinite central concentration, the solution approaches the \lq\lq{}singular isothermal sphere\rq\rq:
\begin{equation}
  \rho=\frac{Aa^2}{4\pi{}Gr^2}\,,\qquad M=\frac{Aa^2}{G}r\,,\label{eq:sism}
\end{equation}
where $M(r)$ is the mass enclosed in $r$, and $A$ is a constant.

Generally, hydrostatic equilibrium of a Bonnor-Ebert sphere that is bounded by an external pressure $P_\mathrm{ext}$ is possible if the cloud mass is less than the critical Bonnor-Ebert mass: 
\begin{equation}
  M_\textrm{crit}=1.18\frac{a^4}{G^{3/2}}P_\mathrm{ext}^{-1/2}\,.\label{eq:mcrit}
\end{equation}
An isothermal singular sphere bounded by an external pressure $P_\mathrm{ext}$ has a maximum radius $R$ and enclosed mass
\begin{equation}
  R=\frac{a^2}{\sqrt{4\pi{}G}}\sqrt{\frac{A}{P_\mathrm{ext}}}\,, \qquad M(R)=\frac{a^4}{\sqrt{4\pi{}G^3}}\sqrt{\frac{A^3}{P_\mathrm{ext}}}\,. \label{eq:Mtot}
\end{equation}

A Bonnor-Ebert sphere that is in hydrostatic equilibrium, can be in unstable or stable equilibrium. If it is in unstable equilibrium it will collapse upon perturbation. The criterion for unstable equilibrium depends on the degree of central concentration. A bounded isothermal singular sphere may or may not be in hydrostatic equilibrium, depending on the value of $A$. If it is in hydrostatic equilibrium, it is in unstable hydrotatic equilibrium. An unbounded isothermal singular sphere (with $p_\mathrm{ext}=0$) is in unstable hydrostatic equilibrium.

The initial conditions for the isothermal self-similar collapse solution require the total mass of the cloud $M_\textrm{cloud}>{}M_\textrm{crit}$.


\subsection{Equations in dimensionless form}

In order to look for self-similar solutions, we introduce the similarity variable $x$, and chose units where $a=1$: 
\begin{equation}\label{eq:units}
\begin{aligned}
  v&=u/a\,, & x&=r/at\,, & p&=P\cdot{}\frac{4\pi{}Gt^2}{a^2}\,,\\
  \psi&=\phi/a^2\,, & \alpha&=\rho\cdot4\pi{}Gt^2\,, & m&=\frac{MG}{a^3t}\,.
\end{aligned}
\end{equation}
The variables above are dimensionless, and only depend on $x$ (not on t). Any equation that can be written solely in the above quantities may produce a self-similar solution.

In these units the Lane-Embden equation becomes
\begin{equation}
  \frac{1}{x^2}\frac{\ud}{\ud x}\left(x^2\frac{\ud \psi}{\ud x}\right)=e^{-\psi}\,,\label{eq:laneemdenn}
\end{equation}
and the singular isothermal solution reads
\begin{equation}
  \alpha(x)=\frac{A}{x^2}\,,\qquad m(x)=Ax\,.\label{eq:sismn}
  %v(x)=-\frac{A-2}{x}\,.\label{eq:sisvn}
\end{equation}
The critical Bonnor-Ebert mass is
\begin{equation}
  m_\textrm{crit}=1.18\cdot2\sqrt{\pi}p_\textrm{ext}^{-1/2}\,.\label{eq:mcritn}
\end{equation}
The total extent $X$ and enclosed mass $m$ of a singular isothermal solution if bounded by an external pressure $p_\mathrm{ext}$
\begin{equation}
  X=\sqrt{\frac{A}{p_\mathrm{ext}}}\,,\qquad m(X)=\sqrt{\frac{A^3}{p_\mathrm{ext}}}\label{eq:mtot}
\end{equation}
Note that the relation between mass, $m$, enclosed within a sphere of radius $x$ and density $\alpha$ in these units is
\begin{equation}
  m=\alpha\frac{x^3}{3}\,.
\end{equation}

\section{Spherically symmetric collapse of an isothermal, self-gravitating cloud}\label{sec:sscis}
This section shows quasi-selfsimilar solutions to the conservation equations for spherically symmetric flow with initial conditions that respresent a singular isothermal sphere.

\subsection{Time dependent conservation equations}
For spherical flow, mass conservation in mass shells reads

\begin{gather}
  \frac{\partial M}{\partial t}+u\frac{\partial M}{\partial r}=0\,, \\
  \frac{\partial M}{\partial t}=-4\pi{}r^2\rho{}u\,, \qquad \frac{\partial M}{\partial r}=4\pi{}r^2\rho\,. \label{eq:massc} 
\end{gather}
In the dimensionless units (Eqn.~\eqref{eq:units}) these become
\begin{gather}
  t\frac{\ud m}{\ud t}+x\frac{\ud m}{\ud x}=0\,, \\
  m-x\frac{\ud m}{\ud x}+v\frac{\ud m}{\ud x}=0\,, \qquad \frac{\ud m}{\ud x}=x^2\alpha\,, \label{eq:masscn}
\end{gather}
and from Eqns.~\eqref{eq:masscn} we obtain
\begin{equation}
  m=x^2\alpha(x-v)\,.
\end{equation}
The continuity equation and the equation of momentum conservation
\begin{gather}
  \frac{\partial \rho}{\partial t}+\frac{1}{r^2}\frac{\partial r^2\rho{}u}{\partial r}=0\,, \\
  \frac{\partial u}{\partial t}+u\frac{\partial u}{\partial r}+\frac{a^2}{\rho}\frac{\partial \rho}{\partial r}+\frac{GM}{r^2}=0\,, 
\end{gather}
in dimensionless units are
\begin{gather}
  \left[(x-v)^2-1\right]\frac{\ud v}{\ud x}=\left[\alpha(x-v)-\frac{2}{x}\right](x-v)\,, \\
  \left[(x-v)^2-1\right]\frac{1}{\alpha}\frac{\ud\alpha}{\ud x}=\left[\alpha-\frac{2}{x}(x-v)\right](x-v)\,.
\end{gather}

\subsection{Solutions}
We wish to solve the coupled set of non-linear ordinary differential equations
\begin{equation}
  \left| 
  \begin{gathered}
    \left[(x-v)^2-1\right]\frac{\ud v}{\ud x}=\left[\alpha(x-v)-\frac{2}{x}\right](x-v) \\
    \left[(x-v)^2-1\right]\frac{1}{\alpha}\frac{\ud\alpha}{\ud x}=\left[\alpha-\frac{2}{x}(x-v)\right](x-v) \\
    m=x^2\alpha(x-v) 
  \end{gathered}
  \right|\,.
  \label{eq:sys}
\end{equation}
We first look at non-trivial singular solutions, point out the asymptotic behaviour for $x\rightarrow0$ and $x\rightarrow\infty$, and then show integrated solutions.

\subsubsection{Singular solution}
The system \eqref{eq:sys} has two non-trivial singular solutions. One is
\begin{equation}
  v=0\,,\qquad\alpha=2/x^2\,,\qquad{}m=2x\,, \label{eq:ssol1}
\end{equation}
and is the isothermal singular sphere (see Eqn.~\eqref{eq:sismn}), which can be scaled by the constant $A$ (integration constant). The other singular solution is
\begin{equation}
  x-v=1\,,\qquad\alpha=2/x\,,\qquad{}m=x\,, \label{eq:ssol2}
\end{equation}
and physically represents a critical point, analogous to a sonic point. For a flow to pass smoothly through this point, Eqn.~\ref{eq:ssol2} must be satisfied.

The isothermal collapse solutions of interest for us require $v<0$ and $\ud v/\ud x<0$ everywhere as well as the absence of a critical point.

\subsubsection{Asymptotic behaviour}
In the limit of negligible velocities ($x\rightarrow\infty$) and $x\rightarrow\infty$ the solution to Eqns.~\eqref{eq:sys} has the asymptotic behaviour of an isothermal singular sphere:
\begin{equation}
  \alpha\rightarrow\frac{A}{x^2}\,, \qquad  m\rightarrow Ax\,, \qquad v\rightarrow-\frac{A-2}{x}\,.\label{eq:sisvna}
\end{equation}
Note that we require $A\geq2$ for inflow.

In the limit of $x\rightarrow0$ the solution approaches asymptotically the characteristic free fall profile.
\begin{equation}
  m\rightarrow{}m_0\,, \qquad \alpha(x)\rightarrow\sqrt{\frac{m_0}{2x^3}}\,,\qquad v\rightarrow-\sqrt{\frac{2m_0}{x}}\,,\label{eq:vna0}
\end{equation}
where $m_0$ is the mass that has already fallen into the core. At time $t=0$ this will be 0 but for $t>0$ at $x=0$, $m_0$ is generally non-zero, and positive if $A\geq2$. $A$ and $m_0$ are implicitly related and their corresponding values can be found by integration of Eqns.~\eqref{eq:sys}. As boundary conditions for the integration one can take values at large or small $x$. 

$A=2$ represents the special case of the expansion-wave collapse solution, the only infall solution for which a critical point is allowed. The critical point for the expansion-wave collapse solution is at $x=1$ and marks the head of an expansion wave within which ($x<1$, i.e. $r<at$) matter is collapsing and beyond which everything is still in (marginal) hydrostatic equilibium.


\subsubsection{Integrated solutions}\label{sec:is}
We integrate the set of coupled (non-linear) ordinary differential equations \eqref{eq:sys}. This can be done with a standard Runge-Kutta scheme. For conveninece I have used Mathematica's NDSolve to solve the system. Figure~\ref{fig:xprof} shows the profiles for $v(x)$, $\alpha(x)$, and $m(x)$ for $A=2.0,2.1,2.2,2.3,\dots,2.9$. 

In Fig.~\ref{fig:rprof} we plot the velocity, density, enclosed mass, mass infall rate profiles as a function of radius for times $t$.

\begin{figure}[bh!]
\begin{minipage}[t]{0.5\textwidth}
  \includegraphics[width=\textwidth]{vplot.pdf}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
  \includegraphics[width=\textwidth]{aplot.pdf}
\end{minipage}\\ 
\begin{minipage}[t]{0.5\textwidth}
  \hspace{0.25mm}
  \includegraphics[width=\textwidth]{mplot.pdf}
\end{minipage}\hspace{0.1\textwidth}
\begin{minipage}[b]{0.4\textwidth}
  \caption{Normalised velocity, density, and enclosed mass profiles of isothermal self-similar spherical collapse solutions to equations~\eqref{eq:sys}. The abscissa is the similarity variable $x=r/at$. The profiles are for a range of values $A=2.0,2.1,2.2,2.3,\dots,2.9$ from bottom to top at the intercept with the ordinate. The dashed lines represent the limit of the free-fall profile as $x\rightarrow0$ and the limit of the singular isothermal sphere for $x\rightarrow\infty$.}\label{fig:xprof}
\end{minipage}
\end{figure}

\begin{figure}[th!]
\begin{minipage}[t]{0.5\textwidth}
  \includegraphics[width=\textwidth]{uplotA20.pdf}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
  \includegraphics[width=\textwidth]{dplotA20.pdf}
\end{minipage}\\[5mm]
\begin{minipage}[t]{0.5\textwidth}
  \includegraphics[width=\textwidth]{MplotA20.pdf}
\end{minipage}\hspace{0.1\textwidth}
\begin{minipage}[b]{0.5\textwidth}
  \includegraphics[width=\textwidth]{MdotplotA20.pdf}
\end{minipage}\\
  \caption{Velocity, density, enclosed mass, and mass infall rate profiles of isothermal self-similar spherical collapse solutions to equations~\eqref{eq:sys} in physical units. The abscissa is the radius $r$. The profiles are a time sequence starting at at $t=1\times10^6\yr$ (dark blue line, lowest of velocity profiles and mass profiles, highest of density profiles) in steps $t=1\times10^6\yr$.}\label{fig:rprof}
\end{figure}

\section{Discussion}\label{sec:disc}
The self-similar isothermal collapse model is inaccurate near the boundary of the cloud, as well as near the core of the cloud. The initial conditions require a singular isothermal sphere that is not in hydrostatic equilibrium, but not far from it. The $r^{-2}$ pressure and density structure provides a balance between pressure force and gravity. It is assumed that the initial conditions are reached in a prior subsonic phase in which the profile adjusts with negligible fluid velocities. This requires the cloud to be near gravitational stability for times $t<0$. Furthermore, for pressure and density gradients to be maintained within the cloud, the conditions must be such that the sound speed exterior to the cloud is greater than the sound speed interior to the cloud.

The Solutions in Sect.~\ref{sec:is} indicate that for about $10^{11}\Msun$ will have fallen into a sphere of $1\kpc$ within $5\Myr$. This seems to be quite a short time. The Mass infall rate is very sensitive around the critical point, inside which everything is approaching free-fall as $r\rightarrow0$. After $2\Myr$, the critical point is at around $1\kpc$ and the mass infall rate is about $10^8\Msun\yr^{-1}$. After $5\Myr$, the critical point is at around $2.3\kpc$ and the mass infall rate is about $3\times10^8\Msun\yr^{-1}$.

\bibliographystyle{astroads}
\bibliography{aywbib.bib}

\end{document}

