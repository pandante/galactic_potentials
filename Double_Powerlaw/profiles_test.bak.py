import unittest

import hqnfw as hq
import numpy as np
import mathtools as mt


class HQProfilesTester(unittest.TestCase):

    def setUp(self):

        ## Hernquist potential 
        self.a_hq = 4.
        self.rho0_hq = 1.3
        self.dens_floor = 1.e-4
        self.temp_ic = 1.e6


        ## The domain r
        self.xscale = 'log'
        self.npt = 10
        if   self.xscale == 'lin': self.r = np.linspace(0.24, 10.24,self.npt)
        elif self.xscale == 'log': self.r = np.logspace(-2, 3,self.npt)


        ## The domain and temperature IC for higher dimension 
        ## cases to test broadcasting
        self.r2 = np.repeat(self.r[None,:]   , 3, 0)
        self.r3 = np.repeat(self.r2[None,:,:], 3, 0)
        self.t2 = np.repeat(self.temp_ic     , 3, 0)
        self.t3 = np.repeat(self.t2[None,:]  , 3, 0)

        ## Hernquist profile class
        self.hp = hq.HQ(self.rho0_hq, self.a_hq, self.dens_floor)

        
        ## Some solutions
        self.pres_sol = {}
        self.pres_sol['analytic'] = \
                np.array([7.74466014e-05, 7.72932424e-05, 7.71494490e-05, 7.70340628e-05,
                 7.69755501e-05, 7.69649522e-05, 7.69645735e-05, 7.69645709e-05,
                 7.69645709e-05, 7.69645709e-05])

        self.pres_sol['odeint'] = \
                np.array([7.74466014e-05, 7.72889267e-05, 7.71411283e-05, 7.70223080e-05,
                 7.69609623e-05, 7.69491709e-05, 7.69490466e-05, 7.69490462e-05,
                 7.69490461e-05, 7.69490460e-05])

        self.pres_sol['cumtrapz'] = \
                np.array([7.72466986e-05, 7.70563692e-05, 7.68947159e-05, 7.67948554e-05,
                 7.67664909e-05, 7.67648497e-05, 7.67648345e-05, 7.67648339e-05,
                 7.67648337e-05])



    def test_pres_profiles(self):
        """
        Test pressure solution with different dimensios for 
        r array and ic. This tests some broadcasting cases
        for the analytic and cumtrapz methods.
        """

        ## Test with a 1D r array and scalar ic
        pres = self.hp.pres(self.r, self.temp_ic)
        self.assertTrue(np.allclose(pres, self.pres_sol['analytic']))

        pres = self.hp.pres(self.r, self.temp_ic, method='odeint')
        self.assertTrue(np.allclose(pres, self.pres_sol['odeint']))

        pres = self.hp.pres(self.r, self.temp_ic, method='cumtrapz')
        self.assertTrue(np.allclose(pres, self.pres_sol['cumtrapz']))

        ## Test with a 2D r array and scalar ic
        pres = self.hp.pres(self.r2, self.temp_ic)
        print np.array_repr(pres)
        print np.array2string(mt.repeat_dim(self.pres_sol['analytic'], pres))
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['analytic'], pres)))

        pres = self.hp.pres(self.r2, self.temp_ic, method='cumtrapz')
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['cumtrapz'], pres)))
        
        ## Test with a 3D r array and scalar ic
        pres = self.hp.pres(self.r3, self.temp_ic)
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['analytic'], pres)))

        pres = self.hp.pres(self.r3, self.temp_ic, method='cumtrapz')
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['cumtrapz'], pres)))
        
        
        ## Test with a 3D r array and 1D ic
        pres = self.hp.pres(self.r3, self.t2)
        self.assertTrue(np.allclose(pres, mt.match_dim(self.pres_sol['analytic'], pres)))

        pres = self.hp.pres(self.r3, self.t2, method='cumtrapz')
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['cumtrapz'], pres)))
        
        ## Test with a 3D r array and 2D ic
        self.t3 = np.repeat(self.t2[None,:], 3, 0)

        pres = self.hp.pres(self.r3, self.t3)
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['analytic'], pres)))

        pres = self.hp.pres(self.r3, self.t3, method='cumtrapz')
        self.assertTrue(np.allclose(pres, mt.repeat_dim(self.pres_sol['cumtrapz'], pres)))

        

if __name__ == '__main__':
    unittest.main()

