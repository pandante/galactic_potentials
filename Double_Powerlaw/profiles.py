import Double_Powerlaw.double_powerlaw as hq
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as ti
import Tools.physconst as pc
import seaborn
import Tools.norm as norm


# ______________________________________________________________________________________________________________________
# The parameters

# Typical values
#
# Capelo et al 2010 values
#   a_nfw = 22.8
#   a_hq = 1.21
#
# NFW values from Navarro, Ludlow, & Springel
#   a_nfw = 10 ~ 20 kpc h^-1
#   rho0_nfw = 6 ~ 30 x 10^15 Msun Mpc^-3 h^2
# or
#   a_nfw = 15.
#   rho0_nfw = 20 * 1.e15 * pc.msun / pc.Mpc ** 3 / (mu * pc.amu)


# Densities are in cm ^-3, and distances in kpc
# See normalizations below

# Hernquist potential
a_hq = 1.5
rho0_hq = 200.
dens_floor = 0.0

# NFW potential 
a_nfw = 15.
rho0_nfw = 1.

# Hot isothermal gas component
thot = 3.e6
nhot = 1.0

# Radius
npt = 10000
r2 = 10.
r1lin = r2 / npt
r1log = 1.e-3
icr = 4.e-7
rlin = np.linspace(r1lin, r2, npt)
rlin = np.insert(rlin, 0, icr)
rlog = np.logspace(np.log10(r1log), np.log10(r2), npt)
rlog = np.insert(rlog, 0, icr)


# Normalization
mu = 0.60364
nm = norm.PhysNorm(x=pc.kpc, t=pc.kyr, dens=mu * pc.amu,
                   temp=(pc.kpc / pc.kyr) ** 2 * pc.amu / pc.kboltz, curr=1)
# nm = norm.PhysNorm(x=pc.kpc, v=pc.c, dens=mu * pc.amu,
#                            temp=pc.c **2 * pc.amu / pc.kboltz, curr=1)

# Velocity dispersion of hot isothermal gas
sigma2 = pc.kboltz * thot / (mu * pc.amu) / nm.v ** 2

# Do plot?
do_plot = True

# Write header?
write_header = True

# Interactive mode
# pl.ion()


# ______________________________________________________________________________________________________________________
# Main bit

# Hernquist + NFW class
hqnfw = hq.HQNFW(a_hq, rho0_hq, a_nfw, rho0_nfw, dens_floor, nm)

# Plot the results
if do_plot:

    # Density profiles
    fg, ax = pl.subplots(2, 1, sharex=True, squeeze=True)
    pl.axes(ax[0])
    pl.plot(np.log10(rlog), np.log10(hqnfw.dens(rlog)), label='HQNFW')
    pl.plot(np.log10(rlog), np.log10(hqnfw.hq.dens(rlog)), 'g--', label='HQ')
    pl.plot(np.log10(rlog), np.log10(hqnfw.nfw.dens(rlog)), 'm--', label='NFW')
    pl.plot(np.log10(rlog), np.log10(nhot * np.exp(-hqnfw.phi(rlog) / sigma2)), 'r-', lw=1.4, label='Hot ISM')
    pl.ylabel(r'$\log\,n\,(\mathrm{cm}^{-3})$', size=18)
    ax[0].xaxis.set_minor_locator(ti.MultipleLocator(0.1))
    ax[0].yaxis.set_minor_locator(ti.MultipleLocator(0.2))
    pl.xlim((-3, 1))
    pl.ylim((-2., 6.))
    pl.legend(prop={'size': 11})
    pl.grid()

    # Potential profiles
    pl.axes(ax[1])
    pl.plot(np.log10(rlog), hqnfw.phi(rlog) / sigma2, label='HQNFW')
    pl.plot(np.log10(rlog), hqnfw.hq.phi(rlog) / sigma2, 'g--', label='HQ')
    pl.plot(np.log10(rlog), hqnfw.nfw.phi(rlog) / sigma2, 'm--', label='NFW')
    pl.xlabel(r'$\log\,r\,(\mathrm{kpc})$', size=18)
    pl.ylabel(r'$\phi/\sigma^2$', size=18)
    pl.xlim((-3, 1))
    pl.ylim((-0.5, 4.2))
    ax[1].xaxis.set_minor_locator(ti.MultipleLocator(0.1))
    ax[1].yaxis.set_minor_locator(ti.MultipleLocator(0.2))
    pl.grid()

    pl.subplots_adjust(hspace=0.02, right=0.98, top=0.98)

    pl.savefig('hqnfw.png', dpi=300)


# Data array to be saved. Everything in cgs.
rlin_f = rlin * nm.x

phi = hqnfw.phi(rlin)
phi_f = phi * nm.v * nm.v

gradphi = hqnfw.grad_phi(rlin)
gradphi_f = gradphi * nm.v / nm.t

rho = nhot * np.exp(-phi / sigma2)
rho_f = rho * nm.dens

pres = rho * thot / nm.temp / mu
pres_f = pres * nm.pres

sv_h = np.vstack((rlin_f, rho_f, pres_f)).T
sv_g = np.vstack((rlin_f, phi_f, gradphi_f)).T

# File name and write to file. Number of lines after header, then the first
# row, whose data has a largest significant digit at a decimal place smaller
# than the width specified in fmt, i.e., colw. This, essentially gives the
# boundary values at 0.
fname = 'hqnfw_' + str(a_hq) + '_' + str(rho0_hq) + \
        '_' + str(a_nfw) + '_' + str(rho0_nfw) + \
        '_t' + format(thot / 1.e7, '.1f') + 'e7' + '_n' + format(nhot, '.1f')

fh_h = open(fname + '-h.dat', 'w')
fh_g = open(fname + '-g.dat', 'w')

# Formatting
colw = '22'
colp = '8'

# Create headers
if write_header:
    hd_h = format('# r', '<' + colw + 's') + format('dens hot', '<' + colw + 's') + \
         format('pres hot', '<' + colw + 's')

    hd2_h = format('#(cm)', '<' + colw + 's') + format('(g cm ^-3)', '<' + colw + 's') + \
          format('(dyn)', '<' + colw + 's')

    hd_g = format('# r', '<' + colw + 's') + format('phi tot', '<' + colw + 's') + \
         format('dphi/dr tot', '<' + colw + 's')

    hd2_g = format('#(cm)', '<' + colw + 's') + format('((cm/s)^2)', '<' + colw + 's') + \
          format('(cm/s^2)', '<' + colw + 's')

    fh_h.write(hd_h + '\n')
    fh_h.write(hd2_h + '\n')
    fh_h.write('6\n')
    fh_h.write('a_hq = ' + str(a_hq) + '\n')
    fh_h.write('rho0_hq = ' + str(rho0_hq) + '\n')
    fh_h.write('a_nfw = ' + str(a_nfw) + '\n')
    fh_h.write('rho0_nfw = ' + str(rho0_nfw) + '\n')
    fh_h.write('nhot = ' + str(nhot) + '\n')
    fh_h.write('thot = ' + str(thot) + '\n')
    fh_h.write(str(npt + 1) + ' 5\n')

    fh_g.write(hd_g + '\n')
    fh_g.write(hd2_g + '\n')
    fh_g.write('6\n')
    fh_g.write('a_hq = ' + str(a_hq) + '\n')
    fh_g.write('rho0_hq = ' + str(rho0_hq) + '\n')
    fh_g.write('a_nfw = ' + str(a_nfw) + '\n')
    fh_g.write('rho0_nfw = ' + str(rho0_nfw) + '\n')
    fh_g.write('nhot = ' + str(nhot) + '\n')
    fh_g.write('thot = ' + str(thot) + '\n')
    fh_g.write(str(npt + 1) + ' 5\n')

np.savetxt(fh_h, sv_h, fmt='%-' + colw + '.' + colp + 'e', delimiter='')
np.savetxt(fh_g, sv_g, fmt='%-' + colw + '.' + colp + 'e', delimiter='')
fh_h.close()
fh_g.close()
print('File written: ' + fname + '-h.dat')
print('File written: ' + fname + '-g.dat')

# Check hydrostatics
dpdr = np.diff(pres) / np.diff(rlin)
ndpsi = rho * gradphi
ratio = -dpdr / (ndpsi[0:-1] + 0.5 * np.diff(ndpsi))
print('hydrostatics - differences')
print('max = ' + str(np.nanmax(np.abs(1 - ratio))))
print('mean = ' + str(np.nanmean(np.abs(ratio))))

