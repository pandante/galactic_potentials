import norm

import hqnfw as hq
import numpy as np
import mathtools as mt

import physconst as pc

import matplotlib.pyplot as pl

## This script is for interactive use to test 
## functions in hqnfw. A proper tester is constructed in profiles_test.py

norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165*pc.amu, 
                        temp=pc.c**2*pc.amu/pc.kboltz, curr=1)

## The domain r
xscale = 'log'
npt = 100
if   xscale == 'lin': r = np.linspace(0.24, 10.24,npt)
elif xscale == 'log': r = np.logspace(-2, 3,npt)

## Default Hernquist potential 
#a_hq = 1.21
a_hq = 4.
rho0_hq = 1.3
a_nfw = 23
rho0_nfw = 0.27
dens_floor = 0.
teic = 1.e6
ric = r[0]
method = 'analytic'

## Filenames of solutions
fname_pres_a = 'hq_pres_analytic.dat'
fname_pres_o = 'hq_pres_odeint.dat'
fname_pres_c = 'hq_pres_cumtrapz.dat'
fname_temp_a = 'hq_temp_analytic.dat'
fname_temp_c = 'hq_temp_cumtrapz.dat'
fname_gracc  = 'hq_gracc.dat'
fname_mass   = 'hq_mass.dat'


## Create 
hp = hq.HQNFW(a_hq, rho0_hq, a_nfw, rho0_nfw, dens_floor, norm)
dens   = hp.dens(r)
dens_hq   = hp.hq.dens(r)
dens_nfw   = hp.nfw.dens(r)
pres   = hp.pres    (r, teic, ric)
temp   = hp.temp_cgs(r, teic, ric)
gracc  = hp.gracc   (r)
mass   = hp.mass    (r)


## Plot it

## Density
pl.figure()
pl.plot(np.log10(r),np.log10(dens))
pl.plot(np.log10(r),np.log10(dens_hq))
pl.plot(np.log10(r),np.log10(dens_nfw))

pl.show(block=False)

## Pressure
pl.figure()
pl.plot(np.log10(r),np.log10(temp))
pl.twinx()
pl.plot(np.log10(r),np.log10(pres))
pl.show(block=False)

