import Double_Powerlaw.double_powerlaw as pot
import numpy as np
import matplotlib.pyplot as pl

ngrid = 100
a = np.logspace(0, 2, ngrid)
rho0 = np.logspace(-1.5, 2, ngrid)
#a = np.array([0.1,1])
#rho0 = np.array([1,10])
d_a, d_rho0 = np.meshgrid(a, rho0)
hp = pot.HQ(d_rho0, d_a)

a_nfw = 40.*np.ones_like(d_a)
rho0_nfw = 0.015*np.ones_like(d_rho0)

temp_ic = 1.e5

hqnfw = pot.HQNFW(d_rho0, d_a, rho0_nfw, a_nfw, 1.e-4)

rho_01 = hp.dens(.1)
rho_1 = hp.dens(1.)
rho_10 = hp.dens(10.)
npt = 1000
rint = np.logspace(-2,0,npt)
rint10 = np.logspace(-2,10,npt)
#te_01 = hqnfw.temp_hydrostatic_cgs(rint)
te_1 = hqnfw.temp_hydrostatic_cgs_vec(rint, temp_ic)
#te_10 = hqnfw.temp_hydrostatic_cgs_vec(rint10, temp_ic)
#pr_01 = hqnfw.pres_hydrostatic_vec(.1)
#pr_1 = hqnfw.pres_hydrostatic_vec(rint)
#pr_10 = hqnfw.pres_hydrostatic(10.)
mass_100 = hqnfw.mass(1000.)
massb_100 = hqnfw.hq.mass(1000.)



pl.figure(1)

rho_01_min = 1.e0
rho_01_max = 1.e3
pl.contour(np.log10(a), np.log10(rho0), np.log10(rho_01),
            alpha=0.4, colors='k', label=r'$\rho(1\,\mathrm{kpc})$',
            levels=[np.log10(rho_01_min), np.log10(rho_01_max)])

#raw_input('Press enter to continue')

rho_1_min = 1.e-1
rho_1_max = 1.e1
pl.contourf(np.log10(a), np.log10(rho0), np.log10(rho_1),
            alpha=0.4, colors='Chocolate', label=r'$\rho(1\,\mathrm{kpc})$',
            levels=[np.log10(rho_1_min), np.log10(rho_1_max)])

#raw_input('Press enter to continue')

rho_10_min = 1.e-4
rho_10_max = 1.e-1
pl.contourf(np.log10(a), np.log10(rho0), np.log10(rho_10),
            alpha=0.4, colors='k', label=r'$\rho(10\,\mathrm{kpc})$',
            levels=[np.log10(rho_10_min), np.log10(rho_10_max)])

raw_input('Press enter to continue')

te_1_min = 1.e5
te_1_max = 1.e7
pl.contourf(np.log10(a), np.log10(rho0), np.log10(te_1),
            alpha=0.4, colors='c', label=r'$T(1\,\mathrm{kpc})$',
            #alpha=0.4, label=r'$T(1\,\mathrm{kpc})$',
            levels=[np.log10(te_1_min), 
                    0.5*(np.log10(te_1_min) + np.log10(te_1_max)),
                    np.log10(te_1_max)])

raw_input('Press enter to continue')

#pr_1_min = 1.e6
#pr_1_max = 5.e8
#pl.contourf(np.log10(a), np.log10(rho0), np.log10(pr_1),
#            alpha=0.8, cmap=pl.cm.jet, label=r'$T(1\,\mathrm{kpc})$',
#            #levels=[np.log10(pr_1_min), np.log10(pr_1_max)])
#            )
#pl.colorbar()
#
#raw_input('Press enter to continue')

#pr_10_min = 1.e6
#pr_10_max = 5.e7
#pl.contourf(np.log10(a), np.log10(rho0), pr_10,
#            alpha=0.8, cmap=pl.cm.jet, label=r'$T(10\,\mathrm{kpc})$',
#            #levels=[np.log10(pr_10_min), np.log10(pr_10_max)])
#            )
#pl.colorbar()

#raw_input('Press enter to continue')

#te_10_min = 1.e6
#te_10_max = 5.e8
#pl.contourf(np.log10(a), np.log10(rho0), np.log10(te_10),
#            alpha=0.4, color='c', label=r'$T(10\,\mathrm{kpc})$',
#            levels=[np.log10(te_10_min), 
#                    0.5*(np.log10(te_10_min) + np.log10(te_10_max)),
#                    np.log10(te_10_max)])

#raw_input('Press enter to continue')

mass_100_min = 1.e11
mass_100_max = 1.e13
pl.contour(np.log10(a), np.log10(rho0), np.log10(mass_100), 
            alpha=0.4, colors='g', linestyless='dashed', label=r'$M(10\,\mathrm{kpc})$',
            levels=[np.log10(mass_100_min), np.log10(mass_100_max)])

raw_input('Press enter to continue')

massb_100_min = 1.e10
massb_100_max = 1.e11
pl.contourf(np.log10(a), np.log10(rho0), np.log10(massb_100), 
            alpha=0.4, colors='y', label=r'$M(10\,\mathrm{kpc})$',
            levels=[np.log10(massb_100_min), np.log10(massb_100_max)])

raw_input('Press enter to continue')


## contourf does not support label
#pl.legend(loc=1, prop={'size':12})

pl.xlabel(r'$\log(a\,[\mathrm{kpc}])$', fontsize=16)
pl.ylabel(r'$\log(\rho_0\,[\mathrm{cm}^{-3}])$', fontsize=16)


ma_rho_01 = np.ma.masked_outside(rho_01, rho_01_min, rho_01_max)
ma_rho_1 = np.ma.masked_outside(rho_1, rho_1_min, rho_1_max)
ma_rho_10 = np.ma.masked_outside(rho_10, rho_10_min, rho_10_max)
ma_te_1 = np.ma.masked_outside(te_1, te_1_min, te_1_max)
#ma_te_10 = np.ma.masked_outside(te_10, te_10_min, te_10_max)
ma_mass_100 = np.ma.masked_outside(mass_100, mass_100_min, mass_100_max)
ma_massb_100 = np.ma.masked_outside(massb_100, massb_100_min, massb_100_max)

def inv_ands(tup):
    res = np.logical_not(tup[0])
    for itup in tup[1:]:
        res = np.logical_and(res, np.logical_not(itup))
    return np.logical_not(res)


ma_ones = np.ones_like(rho_1)
mask = inv_ands((ma_rho_01.mask, ma_rho_1.mask, 
                 ma_rho_10.mask, ma_te_1.mask,
                 ma_mass_100.mask, ma_massb_100.mask))
ma_ones = np.ma.masked_array(ma_ones, mask=mask)
pl.pcolor(np.log10(a), np.log10(rho0), ma_ones)

pl.xlabel(r'$\log(a\,[\mathrm{kpc}])$', fontsize=16)
pl.ylabel(r'$\log(\rho_0\,[\mathrm{cm}^{-3}])$', fontsize=16)

