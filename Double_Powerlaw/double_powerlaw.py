import Tools.physconst as pc
import Tools.norm as norm
import numpy as np
import scipy.integrate as inte
import Tools.eos as eos


class CompositionISM(eos.CompositionBase):
    """
    Currently identical class to Eos/CompositionBase
    """

    def __init__(self, mu=0.6165):
        eos.CompositionBase.__init__(self, mu)


class AlphaBetaGamma():
    """
    Base class for all spherical type potentials which effectively have a double
    power law (gamma at large radii, and beta at small radii)
    given by Hernquist (1990). See also Zhao (2006).

    rho(r) = rho0 / (rs^gamma) [1 + rs^alpha] ^ ((beta - gamma) / alpha)

    rs = r / a

    NOTE:
    Typically, the pressure can only be calculated analytically down 
    to rs ~ 1.e-3 as the difference in small values creates a problem
    in the analytic expression.
    """

    def __init__(self, a, rho0, alpha, beta, gamma, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):

        """
        Initialize Generic potential

        rho0            core (central) density
        a               scale length of halo
        dens_floor      a density floor
        norm            normalization object. Normalization of input
                        (and that of output unless otherwise stated)
 
        tpa is the product  4 pi G rho0 a , which features in 
        many equations. 
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.tpa = self._calc_tpa(self.a, self.rho0)
        self.mu = CompositionISM().mu

        ## The methods available to the pres function to calculate pressure
        self.pres_methods = ['analytic', 'cumtrapz', 'odeint']

    def _calc_tpa(self, a=None, rho0=None, norm=None):
        """
        tpa is the product  4 pi G rho0 a , which features in 
        many equations. G must be normalized.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        inv_unit_G = norm.t * norm.t * norm.dens
        return 4 * np.pi * pc.newton * inv_unit_G * rho0 * a

    def dens(self, r, a=None, rho0=None, alpha=None, beta=None, gamma=None, dens_floor=None):
        """
        The density profile of any spherical double power law potential.
        All derived classes automatically use this function, which is
        convenient, but care must be take about the order of parameters 
        when calling (e.g. alpha, beta, and gamma, come before dens_floor).
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if dens_floor is None: dens_floor = self.dens_floor
        if alpha is None: alpha = self.alpha
        if beta is None: beta = self.beta
        if gamma is None: gamma = self.gamma

        rs = r / a

        dens = np.maximum(rho0 / (rs ** gamma * (1 + rs ** alpha) ** ((beta - gamma) / alpha)), dens_floor)

        return dens


class Dehnen(AlphaBetaGamma):
    """
    Class for Dehnen (1993) type potentials.

    (alpha, beta, gamma) = (1, 4, gamma)
    """

    def __init__(self, a, rho0, gamma, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):
        """
        Initialize Dehnen potential

        rho0        core (central) density
        a           scale length of halo
        dens_floor  a density floor
        gamma       the free power index
        norm        normalization object for input (and output)

        tpa is the product  4 pi G rho0 a , which features in 
        many equations. 
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.alpha = 1.0
        self.beta = 4.0

        AlphaBetaGamma.__init__(self, a, rho0, self.alpha, self.beta, gamma,
                                dens_floor, norm)


class Hernquist(Dehnen):
    """ 
    The Hernquist Potential 
    (alpha, beta, gamma) = (1, 4, 1)
    This class requires a normalization object from the norm module
    and a composition object from the eos module.
    """

    def __init__(self, a, rho0, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):
        """
        Initialize Hernquist Potential

        rho0            core (central) density
        a               scale length of halo
        dens_floor      a density floor
        norm            The input parameters are assumed to be 
                        normalized by this

        tpa is the product  4 pi G rho0 a , which features in 
        many equations. 
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.gamma = 1.0

        Dehnen.__init__(self, a, rho0, self.gamma, dens_floor, norm)

    def phi(self, r, a=None, rho0=None, norm=None):
        """
        The potential has an analytic form.
        Constant of integration is chosen such that
        phi(0) = 0
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        tpa = self._calc_tpa(a, rho0, norm)
        phi = -tpa * a * 0.5 / (1. + r / a) + 0.5 * tpa * a

        return phi

    def pres(self, r, teic, ric=None, a=None, rho0=None, norm=None,
             method='analytic'):
        """
        This function returns the pressure profile given a 
        reference point for at ric for which the termperature teic is
        known. This does not need to be the central temperature. 
        
        A combination of the functions pres_eq and _pres_diff is used. 
        pres_eq calculates the analytic profile of the pressure from hydrostatic 
        equilibrium, but that expression has the constant of integration at 0. 
        The difference between the value at ric from pres_eq and that calculated 
        from a temperature using the function _presic_from_temp is obtained from
        _pres_diff. Since all that is required to maintain hydrostatic equilibrium 
        is that the pressure gradient remain the same, we can just add that
        pressure difference to the whole profile.
        """

        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm
        if ric is None: ric = np.ravel(r)[0] * np.ones_like(teic)

        ## Check method
        if method in self.pres_methods is None:
            print('Method ' + method + ' not known. Must be one of ', self.pres_methods)
            raise (ValueError)

        ## Do calculation according to method
        if method == 'analytic':

            pe = self.pres_eq(r, a, rho0, norm)
            pd = self._pres_diff(teic, ric, a, rho0, norm)

            return pe + pd

        elif method == 'odeint':

            ic = self._presic_from_temp(teic, ric, a=a, rho0=rho0, norm=norm)

            result = inte.odeint(self._grad_pres, ic, r, Dfun=self._grad_pres_dfun)

            return result.squeeze()

        elif method == 'cumtrapz':

            ic = self._presic_from_temp(teic, ric, a=a, rho0=rho0, norm=norm)

            result = np.insert(inte.cumtrapz(self.grad_pres(r), r), 0, 0, axis=-1) + ic

            return result

    def pres_eq(self, r, a=None, rho0=None, norm=None):
        """
        The analytic expression for the pressure profile from
        hydrostatic equilibrium. This expression has set the
        integration constant to 0, so, on it's own is not useful
        in providing a realistic pressure profile. The pressure 
        profile may be offset arbitrarily to, e.g., give a good 
        temperature profile.

        The function _pres_diff, e.g., calculates the offset difference 
        between the value returned by this here function and that for a 
        temperature data point at r. This difference is added to this 
        function in the function pres.

        rs is the scaled radius.
        """

        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        tpa = self._calc_tpa(a, rho0, norm)
        rs = r / a
        pres = (-tpa * rho0 * a * 0.5 * ((1. + rs) ** (-4) / 12. *
                                         (25. + 52. * rs + 42. * rs * rs + 12. * rs * rs * rs) -
                                         np.log(1. + rs) + np.log(rs)))
        return pres

    def _presic_from_temp(self, teic, ric, a=None, rho0=None, dens_floor=None,
                          norm=None):
        """
        The pressure from the temperature teic at a point ric using 
        an ideal equation of state and the value of mu saved in 
        this object.

        Assumes teic and ric are broadcastable.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        ## The arguments before dens_floor are alpha, beta, gamma
        ## so need to specify dens_floor keyword 
        dens = self.dens(ric, a, rho0, dens_floor=dens_floor)

        return dens * teic / self.mu / norm.temp

    def _pres_diff(self, teic, ric, a=None, rho0=None, norm=None):
        """
        The difference in pressure between that obtained from 
        a given temperature (using function _presic_from_temp) 
        and that obtained from hydrostatic equilibrium assuming 
        with an integration constant = 0 (using function pres_eq).
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        preseq = self.pres_eq(ric, a, rho0, norm)
        presic = self._presic_from_temp(teic, ric, a, rho0, norm=norm)

        return presic - preseq

    def grad_phi(self, r, a=None, rho0=None, norm=None):
        """
        Returns grad phi. This is an analytic expression.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        tpa = self._calc_tpa(a, rho0, norm)

        return 0.5 * tpa / (1. + r / a) ** 2

    def _grad_pres(self, x, r, a=None, rho0=None, norm=None):
        """
        Returns pressure from gradient of potential assuming hydrostatic
        equilibium, grad pres = -rho grad phi.

        This is the function used for odeint. x is the dependent variable, r
        the independent variable.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        return -self.dens(r, a, rho0) * self.grad_phi(r, norm=norm)

    def grad_pres(self, r, a=None, rho0=None, norm=None):
        """
        Returns pressure from gradient of potential assuming hydrostatic
        equilibium, grad pres = -rho grad phi.

        Note:
        The function _grad_pres is used for odeint. x is the dependent variable, r
        the independent variable.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        return self._grad_pres(0, r, a, rho0, norm)

    def _grad_pres_dfun(self, x, r):
        """
        The jacobian of _grad_pres with respect to x 
        (which may in general be an array).
        r is the independent variable and also needs to
        be part of the call syntax.

        For grad_pres, nothing on the RHS of the 
        differential equation depends on x, so we
        just return zero (but as an array)
        """

        return np.array([0])

    def temp_cgs(self, r, teic, ric=None, a=None, rho0=None, norm=None,
                 dens_floor=None, method='analytic'):
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm
        if dens_floor is None: dens_floor = self.dens_floor

        rs = r / a
        pres = self.pres(r, teic, ric, a, rho0, norm, method)
        dens = self.dens(r, a, rho0, dens_floor=dens_floor)

        return pres / dens * self.mu * self.norm.temp

    def gracc(self, r, a=None, rho0=None, norm=None):
        """
        Returns a positive gravitational acceleration inward.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        return -self.grad_phi(r, a, rho0, norm)

    def mass(self, r, a=None, rho0=None, norm=None):
        """
        Returns confined mass in units of solar mass

        NOTE:
        Does not repsect dens_floor.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm
        rs = r / a
        mass = 4 * np.pi * rho0 * a ** 3 * rs * rs / (2 * (1 + rs) ** 2) * norm.m / pc.msun
        return mass


class MGM(AlphaBetaGamma):
    """
    Base class for the class of potentials studied by 
    Merrit, Graham, Moore (2006).

    Like Dehnen, but (alpha, beta, gamma) = (1, 3, gamma)
    """

    def __init__(self, a, rho0, gamma, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):
        """
        Initialize Dehnen potential

        rho0        core (central) density
        a           scale length of halo
        dens_floor  a density floor
        gamma       the free power index
        norm        normalization object

        tpa is the product  4 pi G rho0 a , which features in 
        many equations. 
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.alpha = 1.0
        self.beta = 3.0

        AlphaBetaGamma.__init__(self, a, rho0, self.alpha, self.beta, gamma,
                                dens_floor, norm)


class NFW(MGM):
    """
    NFW profile.

    (alpha, beta, gamma) = (1, 3, 1)
    """

    def __init__(self, a, rho0, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):
        """
        Initialize NFW Potential

        rho0            core (central) density
        a               scale length of halo
        dens_floor      density floor
        norm            The input parameters are assumed to be 
                        normalized by this

        tpa is the product  4 pi G rho0 a , which features in 
        many equations. 
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.gamma = 1.0

        MGM.__init__(self, a, rho0, self.gamma, dens_floor, norm)

    def phi(self, r, a=None, rho0=None, norm=None):
        """
        The potential has an analytic form.
        Constant of integration is chosen such that
        phi(0) = 0
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        rs = r / a
        tpa = self._calc_tpa(a, rho0, norm)
        phi = -tpa * a * np.log(1 + rs) / rs + tpa * a

        return phi

    def grad_phi(self, r, a=None, rho0=None, norm=None):
        """
        Returns grad phi. This is an analytic expression.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        tpa = self._calc_tpa(a, rho0, norm)
        rs = r / a
        return tpa * ((1 + rs) * np.log(1 + rs) - rs) / (rs * rs * (1 + rs))

    def mass(self, r, a=None, rho0=None, norm=None):
        """
        Returns the contained mass analytically.

        NOTE:
        Does not respect dens_floor.
        """
        if a is None: a = self.a
        if rho0 is None: rho0 = self.rho0
        if norm is None: norm = self.norm

        tpa = self._calc_tpa(a, rho0, norm)

        rs = r / a
        mass = 4 * np.pi * rho0 * a ** 3 * (np.log(1 + rs) - rs / (1 + rs)) * \
               norm.m / pc.msun
        return mass


class HQNFW():
    """
    This is a combined Hernquist and NFW profile. Note that since it 
    *is* neither, it doesn't inherit from either. It's just a container
    class for the two, that also has convenience functions to calculate
    the potential, hydrostatic pressure, etc.
    """

    def __init__(self, a_hq, rho0_hq, a_nfw, rho0_nfw, dens_floor=0.,
                 norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165 * pc.amu,
                                    temp=pc.c ** 2 * pc.amu / pc.kboltz, curr=1)):
        """
        norm        The input parameters are assumed to be 
                    normalized by this
        """

        self.__dict__.update(locals());
        del self.__dict__['self']

        self.hq = Hernquist(a_hq, rho0_hq, dens_floor, norm)
        self.nfw = NFW(a_nfw, rho0_nfw, dens_floor, norm)

    def dens(self, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None,
             dens_floor=None, norm=None):
        """
        The density is just the sum of the densities of the two components..
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        dens = self.hq.dens(r, a_hq, rho0_hq, dens_floor=0) + \
               self.nfw.dens(r, a_nfw, rho0_nfw, dens_floor=0)
        dens = np.maximum(dens, dens_floor)

        return dens

    def _poisson(self, x, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None,
                 dens_floor=None, norm=None):
        """
        Variables
        x[0] = phi
        x[1] = g

        Equations
        dphi/dr = g
        dg/dr = 4 pi G rho(r) - 2g/r
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        return np.array([x[1], 4 * np.pi * pc.newton * self.inv_unit_G *
                         self.dens(r, a_hq, rho0_hq, a_nfw, rho0_nfw,
                                   dens_floor, norm) -
                         2 * x[1] / r])

    def _jacobian(self, x, r):
        """
        x[0] = phi
        x[1] = g

        Equations
        dphi/dr = g
        dg/dr = 4 pi G rho(r) - 2g/r
        """
        return np.array([[0, 1], [0, -2. / r]])

    def phi_poisson(self, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None,
                    dens_floor=None, norm=None):
        """
        Potential calculatied by solving Poisson's equation as a 
        system of two ODE. Just for demonstration

        Equations
        dphi/dr = g
        dg/dr = 4 pi G rho(r) - 2g/r

        Vars
        x[0] = phi
        x[1] = g

        NOTE: this way of calculating does not allow for ...?
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        ## The initial conditions
        # ic0 = -1.
        ic0 = self.hq.phi(r[0])
        ic1 = self.hq.grad_phi(r[0], r[1] - r[0])
        ic = (ic0, ic1)

        ## The integration
        res = inte.odeint(self._poisson, ic, r, Dfun=self._jacobian,
                          args=(a_hq, rho0_hq, a_nfw, rho0_nfw, dens_floor, norm))
        return res[:, 0]

    def phi(self, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None, norm=None):
        """
        Potential is just sum of NFW and Hernquist potentials 
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if norm is None: norm = self.norm

        phi = self.hq.phi(r, a_hq, rho0_hq, norm) + \
              self.nfw.phi(r, a_nfw, rho0_nfw, norm)

        return phi

    def grad_phi(self, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None, norm=None):
        """
        Since the combined potential is just the sum of the two potentials, so
        are the gradients. 
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if norm is None: norm = self.norm

        return self.hq.grad_phi(r, a_hq, rho0_hq, norm) + \
               self.nfw.grad_phi(r, a_nfw, rho0_nfw, norm)

    def grad_pres(self, r, a_hq=None, rho0_hq=None, a_nfw=None,
                  rho0_nfw=None, dens=None, dens_floor=None, norm=None):
        """
        The pressure gradient, -rho dphi/dr. In this case, the pressure comes from
        gas of the baryonic (Hernquist) component, but the gradient must balance
        gravity dphi/dr of the combined potential.

        One can use an independent density disitribution, however, and let it
        gravitate in the HQNFW potential.

        """

        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens is None: dens = self.hq.dens
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        return -dens(r, a_hq, rho0_hq, dens_floor=dens_floor) * \
               self.grad_phi(r, a_hq, rho0_hq, a_nfw, rho0_nfw, norm)

    def pres(self, r, teic, ric=None, a_hq=None, rho0_hq=None, a_nfw=None,
             rho0_nfw=None, dens=None, dens_floor=None, norm=None):
        """ 
        r      radial position (numpy array)
        teic   temperature at ric
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens is None: dens = self.hq.dens
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        if ric is None: ric = np.ravel(r)[0] * np.ones_like(teic)

        ## The initial temperature
        ic = self.hq._presic_from_temp(teic, ric, a_hq, rho0_hq, dens_floor, norm)

        result = np.insert(
            inte.cumtrapz(
                self.grad_pres(r, a_hq, rho0_hq, a_nfw,
                               rho0_nfw, dens,
                               dens_floor, norm), r), 0, 0, axis=-1) + ic

        # return result[:,0]
        return result

    def temp_cgs(self, r, teic, ric=None, a_hq=None, rho0_hq=None, a_nfw=None,
                 rho0_nfw=None, dens_floor=None, norm=None):
        """
        The temperature in cgs.
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        return self.pres(r, teic, ric, a_hq, rho0_hq, a_nfw,
                         rho0_nfw, dens_floor, norm) / \
               self.hq.dens(r, a_hq, rho0_hq, a_nfw,
                            rho0_nfw, dens_floor=dens_floor) * \
               self.hq.mu * self.norm.temp

    def gracc(self, r, a_hq=None, rho0_hq=None, a_nfw=None, rho0_nfw=None,
              dens_floor=None, norm=None):
        """
        Returns a positive gravitational acceleration inward.
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if dens_floor is None: dens_floor = self.dens_floor
        if norm is None: norm = self.norm

        return -self.grad_phi(r, a_hq, rho0_hq, a_nfw, rho0_nfw, norm)

    def mass(self, r, a_hq=None, rho0_hq=None, a_nfw=None,
             rho0_nfw=None, norm=None):
        """
        The contained mass in units of solar masses. This is just the sum
        of the masses calculated from the Hernquist and NFW potentials.

        NOTE
        Does not respect dens_floor
        """
        if a_hq is None: a_hq = self.a_hq
        if rho0_hq is None: rho0_hq = self.rho0_hq
        if a_nfw is None: a_nfw = self.a_nfw
        if rho0_nfw is None: rho0_nfw = self.rho0_nfw
        if norm is None: norm = self.norm

        return self.hq.mass(r, a_hq, rho0_hq, norm=norm) + \
               self.nfw.mass(r, a_nfw, rho0_nfw, norm=norm)
