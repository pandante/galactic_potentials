import unittest
import norm

import hqnfw as hq
import numpy as np
import mathtools as mt

import physconst as pc


class HernquistProfileTester(unittest.TestCase):

    def setUp(self):

        ## Save solutions to file
        self.savetxt = False

        self.norm=norm.PhysNorm(x=pc.kpc, v=pc.c, dens=0.6165*pc.amu, 
                                temp=pc.c**2*pc.amu/pc.kboltz, curr=1)

        ## The domain r
        self.xscale = 'log'
        self.npt = 10
        if   self.xscale == 'lin': self.r = np.linspace(0.24, 10.24,self.npt)
        elif self.xscale == 'log': self.r = np.logspace(-2, 3,self.npt)

        ## Default Hernquist potential 
        self.a = 4.
        self.rho0 = 1.3
        self.dens_floor = 1.e-4
        self.teic = 1.e6
        self.ric = self.r[0]
        self.method = 'analytic'

        ## Filenames of solutions
        self.fname_pres_a = 'hq_pres_analytic.dat'
        self.fname_pres_o = 'hq_pres_odeint.dat'
        self.fname_pres_c = 'hq_pres_cumtrapz.dat'
        self.fname_temp_a = 'hq_temp_analytic.dat'
        self.fname_temp_c = 'hq_temp_cumtrapz.dat'
        self.fname_gracc  = 'hq_gracc.dat'
        self.fname_mass   = 'hq_mass.dat'

        ## Save result in files with default parameters
        ## from this routine
        if self.savetxt: self.write_solutions()


    def write_solutions(self):
        """
        Save result in files with default parameters from setUp.
        """

        r = self.r

        a = self.a
        rho0 = self.rho0
        teic = self.teic
        ric = self.ric
        dens_floor = self.dens_floor
        norm = self.norm

        ## Hernquist profile class
        hp = hq.Hernquist(a, rho0, dens_floor, norm)

        pres_a = hp.pres    (r, teic, ric)
        pres_o = hp.pres    (r, teic, ric, method='odeint'  )
        pres_c = hp.pres    (r, teic, ric, method='cumtrapz')
        temp_a = hp.temp_cgs(r, teic, ric)
        temp_c = hp.temp_cgs(r, teic, ric, method='cumtrapz')
        gracc  = hp.gracc   (r)
        mass   = hp.mass    (r)

        np.savetxt(self.fname_pres_a, pres_a)
        np.savetxt(self.fname_pres_o, pres_o)
        np.savetxt(self.fname_pres_c, pres_c)
        np.savetxt(self.fname_temp_a, temp_a)
        np.savetxt(self.fname_temp_c, temp_c)
        np.savetxt(self.fname_gracc , gracc )
        np.savetxt(self.fname_mass  , mass  )


    def test_pres_profiles(self):
        """
        Test pressure solution with different dimensions for 
        teic, ric, a, and rho0. This tests some broadcasting cases
        for the analytic and cumtrapz methods as well.
        """

        ## Hernquist potential. Pick up the default values.
        r = self.r
        a = self.a
        rho0 = self.rho0
        dens_floor = self.dens_floor
        teic = self.teic
        ric = self.ric
        norm = self.norm

        ## Test all three methods with a 1D r array and scalar ic
        self.calc_and_compare_values(r, a, rho0, teic, ric, dens_floor, norm)


        ## Test broadcastability of three methods. The range r is always the same 
        ## (just 1D) teic can have any shape. So can a and rho. These three
        ## parameters must be matched in dimensions to allow broadcasting. 

        ## Test just higher dim for one parameter (teic)
        teic    = self.teic*np.ones((3))[   :,None]
        r       = self.r                [None,   :]

        self.calc_and_compare_values(r, a, rho0, teic, ric, dens_floor, norm)

        ## More complicated broadcasting
        a       = self.a      *np.ones((5, 6, 7))[   :,   :,   :,None,None,None,None]
        rho0    = self.rho0   *np.ones((2, 4))   [None,None,None,   :,   :,None,None]
        teic    = self.teic   *np.ones((3))      [None,None,None,None,None,   :,None]
        r       = self.r                         [None,None,None,None,None,None,   :]

        self.calc_and_compare_values(r, a, rho0, teic, ric, dens_floor, norm)


    def calc_and_compare_values(self=None, r=None, a=None, rho0=None, teic=None, 
                                ric=None, dens_floor=None, norm=None):

        if r == None: r = self.r
        if a == None: a = self.a
        if rho0 == None: rho0 = self.rho0
        if teic == None: teic = self.teic
        if ric == None: ric = self.ric
        if dens_floor == None: dens_floor = self.dens_floor
        if norm == None: norm = self.norm

        ## Hernquist profile class
        hp = hq.Hernquist(a, rho0, dens_floor, norm)

        ## Test all three methods with a 1D r array and scalar ic
        pres_a = hp.pres    (r, teic, ric)
        pres_c = hp.pres    (r, teic, ric, method='cumtrapz')
        temp_a = hp.temp_cgs(r, teic, ric)
        temp_c = hp.temp_cgs(r, teic, ric, method='cumtrapz')
        gracc  = hp.gracc   (r)
        mass   = hp.mass    (r)

        ## This automatically checks that each element of the 0th dimension of
        ## pres_{a,c} is "close to" the data (which is only one-dimensional)
        ## because allclose broadcasts the arguments (which are broadcastable)
        self.assertTrue(np.allclose(pres_a, np.loadtxt(self.fname_pres_a)))
        self.assertTrue(np.allclose(pres_c, np.loadtxt(self.fname_pres_c)))
        self.assertTrue(np.allclose(temp_a, np.loadtxt(self.fname_temp_a)))
        self.assertTrue(np.allclose(temp_c, np.loadtxt(self.fname_temp_c)))
        self.assertTrue(np.allclose(gracc , np.loadtxt(self.fname_gracc )))
        self.assertTrue(np.allclose(mass  , np.loadtxt(self.fname_mass  )))


suite = unittest.TestLoader().loadTestsFromTestCase(HernquistProfileTester)
unittest.TextTestRunner(verbosity=2).run(suite)

