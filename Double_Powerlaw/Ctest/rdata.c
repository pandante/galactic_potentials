#include "rGravTable.h"
#include <stdio.h>
#include <stdlib.h>


int main(){

  double gr_table[NIGRAV][NJGRAV];
  int i, il;
  double r;
  double fc, intp;
  double y0, y1, y2, y3;


  rGravHQNFW(GRAVFNAME);

  if (rGravTable(GRAVFNAME, gr_table) != NIGRAV){
    printf("Error: Table read unsuccessful.");
    exit(1);
  }

  for (i=0; i<NIGRAV; ++i){
    radg[i] = gr_table[i][0];
    dens[i] = gr_table[i][1];
    pres[i] = gr_table[i][2];
    phi[i]  = gr_table[i][3];
    gphi[i] = gr_table[i][4];
    printf("%e %e %e %e %e \n",
        radg[i], dens[i],
        pres[i], phi[i] , gphi[i]
        );
  }

  r = 5.23;
  il = hunter(radg, NIGRAV, r);

  y0 = radg[il-1];
  y1 = radg[il];
  y2 = radg[il+1];
  y3 = radg[il+2];

  fc = (r - y1)/(y2 - y1);

  printf("il = %d\n", il);
  printf("fc = %f\n", fc);

  printf("Original r = %f\n", r);

  intp = LinearInterpolate(y1, y2, fc);
  printf("LinearInterpolate r = %f\n", intp);

  intp = CosineInterpolate(y1, y2, fc);
  printf("CosineInterpolate r = %f\n", intp);

  intp = CubicInterpolate(y0, y1, y2, y3, fc);
  printf("CubicInterpolate r = %f\n", intp);

  intp = CubicCatmullRomInterpolate(y0, y1, y2, y3, fc);
  printf("CubicCatmullRomInterpolate r = %f\n", intp);

  intp = HermiteInterpolate(y0, y1, y2, y3, fc, 0, 0);
  printf("HermiteInterpolate r = %f\n", intp);

  return 0;
}

