#include "rGravTable.h"

void rGravHQNFW(char fname[]){
  struct ccl_t config;
  const struct ccl_pair_t *iter;
  char * pEnd;

  /* Set configuration file details */
  config.comment_char = '#';
  config.sep_char = '=';
  config.str_char = '"';

  /* Parse the file */
  ccl_parse(&config, fname);

  /* Get all key value pairs */
  potential[AHQ] = strtod(ccl_get(&config, "a_hq"), &pEnd);
  potential[DHQ] = strtod(ccl_get(&config, "rho0_hq"), &pEnd);
  potential[ANFW] = strtod(ccl_get(&config, "a_nfw"), &pEnd);
  potential[DNFW] = strtod(ccl_get(&config, "rho0_nfw"), &pEnd);
  potential[TEIC] = strtod(ccl_get(&config, "temp_ic"), &pEnd);

  /* Clean up */
  ccl_release(&config);
}



int rGravTable(char fname[], double table[][NJGRAV]){
  /*
   * This routine reads a data file that has the structure:
   * - Any number of comment lines starting with #
   * - The number of key,value lines
   * - Specified number of key,value lines of form key = value
   *   This is read by the ccl library
   * - Number of rows and number of columns of data table
   * - Data table
   *
   * It only reads the number of keys, number of rows and columns
   * and the data table. It doesn't read the key,value pairs
   *
   * Example file:
   *
   * ## Comment line 1
   * ## Comment line 2
   * ##  ...
   * 3
   * key1 = val1
   * key2 = val2
   * key3 = val3
   * 100 3
   * data11 data12 data 13
   * data21 data22 data 23
   * data31 data32 data 33
   *     ...
   *
   *
   * Arguments:
   * 
   * fname    File name
   * table    2D array for data table, that is filled
   *          by this routine
   * 
   * Returns:
   *
   * Returns number of rows (read)
   *
   * */

  FILE * f;

  char jb;
  int nkeys, nrow, ncol;
  int i, j, l;

  if ((f = fopen(fname, "r")) == NULL){
    printf("Error: rtable: Unable to open file");
    exit(1);
  }

  /* Skip comments (starting with #)*/
  jb = fgetc(f);
  while (jb == '#'){
    while (fgetc(f) != '\n');
    jb = fgetc(f);
  }
  fseek(f, -sizeof(jb), SEEK_CUR);

  /* Number of keys */
  fscanf(f, "%d", &nkeys);

  /* Skip key,value lines - this is done
   * with ccl */
  for (l=0; l<=nkeys; ++l){
    while (fgetc(f) != '\n');
  }

  /* Number of rows and column to data */
  fscanf(f, "%d %d", &nrow, &ncol);
  while (fgetc(f) != '\n');

  /* Read data */
  for (i=0; i<nrow; ++i){
    for (j=0; j<ncol; ++j){
      fscanf(f, "%le ", &table[i][j]);
    }
  }

  /* Clean up */
  fclose(f);

  return nrow;

}


