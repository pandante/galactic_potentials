
int main(int argc, char * argv[]){

  double * r, * dens, * pres, * phi, * gphi;

  FILE * f;
  char jb;
  char fname[] = "hqnfw.dat";

  int l, nkeys, nrow, ncol;

  struct ccl_t config;
  const struct ccl_pair_t *iter;

  /* Set configuration file details */
  config.comment_char = '#';
  config.sep_char = '=';
  config.str_char = '"';

  /* Parse the file */
  ccl_parse(&config, fname);

  /* Iterate through all key/value pairs */
  while((iter = ccl_iterate(&config)) != 0) {
    printf("(%s, %s)\n", iter->key, iter->value);
  }

  /* Clean up */
  ccl_release(&config);



  f = fopen(fname, "r");

  /* Skip comments (starting with #)*/
  jb = fgetc(f);
  while (jb == '#'){
    while (fgetc(f) != '\n');
    jb = fgetc(f);
  }
  fseek(f, -sizeof(jb), SEEK_CUR);

  /* Number of keys */
  fscanf(f, "%d", &nkeys);
  printf("nkeys = %d\n", nkeys);

  /* Skip key,value lines - this is done
   * with ccl */
  for (l=0; l<=nkeys; ++l){
    while (fgetc(f) != '\n');
  }

  /* Number of rows and column to data */
  fscanf(f, "%d %d", &nrow, &ncol);
  printf("nrow %d ncol %d\n", nrow, ncol);
  while (fgetc(f) != '\n');

  /* Create data arrays */
  r = (double *) malloc(nrow*sizeof(*r));
  dens = (double *) malloc(nrow*sizeof(*r));
  pres = (double *) malloc(nrow*sizeof(*r));
  phi = (double *) malloc(nrow*sizeof(*r));
  gphi = (double *) malloc(nrow*sizeof(*r));

  /* Read data */
  for (l=0; l<nrow; ++l){
    fscanf(f, "%le %le %le %le %le", &r[l], &dens[l], &pres[l], &phi[l], &gphi[l]);
    printf("%e %e %e %e %e\n", r[l], dens[l], pres[l], phi[l], gphi[l]);
  }

  /* Clean up */
  fclose(f);

  return 0;
}
