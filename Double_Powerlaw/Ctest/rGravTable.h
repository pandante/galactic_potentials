#include <stdio.h>
#include <stdlib.h>
#include "ccl/ccl.h"

#define NIGRAV 10
#define NJGRAV 5
#define GRAVFNAME "hqnfw.dat"
double radg[NIGRAV],
       dens[NIGRAV],
       pres[NIGRAV],
       phi[NIGRAV],
       gphi[NIGRAV];

#define NPOT 5
#define DHQ 0
#define AHQ 1
#define DNFW 2
#define ANFW 3
#define TEIC 4
double potential[NPOT];


void rGravHQNFW(char fname[]);
int rGravTable(char fname[], double table[][NJGRAV]);

