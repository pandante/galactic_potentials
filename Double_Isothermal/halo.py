import numpy as np
import matplotlib.pyplot as pl
from scipy import integrate as inte
import physconst as pc
import scipy.interpolate as sin
import scipy.optimize as sop

from cycler import cycler
# pl.style.use(['dark_background', 'presentation'])
# pl.style.use(['dark_background'])
# pl.style.use('tableau-colorblind10')
# pl.style.use('seaborn-colorblind')
pl.style.use('seaborn')
pl.rcParams['font.family'] = 'serif'
pl.rcParams['font.serif'] = 'Times New Roman'
pl.rcParams['font.sans-serif'] = 'Times New Roman'
pl.rcParams['mathtext.fontset'] = 'cm'
# Tableau colorblind 10 palette
tableau10_colors = ['006BA4', 'FF800E', 'ABABAB', '595959', '5F9ED1', 'C85200', '898989', 'A2C8EC', 'FFBC79', 'CFCFCF']
pl.rcParams['axes.prop_cycle'] = cycler(color=['#' + s for s in tableau10_colors])

dark_colors = True
if dark_colors:
    fc = 'black'
    fc_i = 'white'
    fc_n = (0, 0, 0, 0.1)
else:
    fc = 'white'
    fc_i = 'black'
    fc_n = (1, 1, 1, 0.3)

pl.rcParams.update({
    'text.color': fc,
    'axes.labelcolor': fc,
    'xtick.color': fc,
    'ytick.color': fc,
    'xtick.direction': 'in',
    'ytick.direction': 'in',
    'grid.color': fc_i,
    'grid.alpha': 0.5,
    'axes.facecolor': fc_n,
    # 'axes.grid.which': 'both',
    # 'axes.grid.axis': 'both',
    'figure.facecolor': (0, 0, 0, 0),
    # 'figure.edgecolor': 'black',
    'savefig.facecolor': (0, 0, 0, 0),
    })

# ______________________________________________________________________________________________________________________
# The parameters

# Parameters
#
# kappa    ratio of velocity dispersions 
# lam      ratio of core radii/scale lengths
# rho      ratio of densities
# rho_hot  The actual hot phase density at the center.
#          The double isothermal is scalable in density
#          by a constant multiplier. Here in cgs (1./cm3)
# thot     hot phase temperature (K)
# sigmab   Baryonic component velocity dispersion (in km/s)
# mbh      Mass of black hole (in solar masses).
#
# Sutherland & Bicknell use kappa = 2 and lam = 10
# in their fiducial model. thot is 1.2e7 ~ tvir_DM 
# and rho_hot = 8.35e-2. 
# rhod0 and rhob0 are the DM and baryonic matter
# densities at the center of the potential. 
#
# NOTE: that the density distributions of these two
# components are 'formal' in the sense that they
# do not appear in the simulations as such. They
# are introduced in order to derive a realistic potential
# whose underlying density distribution is a two-component
# composite isothermal sphere. The actual hot-phase density
# is hydrostatic wrt the total potential and follows
# nhot = nhot0 * exp(-tvir / thot * phi), with nhot arbitrarily
# chosen.
#
# There is a slight inconsistency with this description at large
# radii as the baryonic component may formally go below the 
# hot phase distribution, but at these radii (for reasonable parameters
# the DM dominates, and the hot phase may, in a sense, 
# represent the IGM background.


# Range in kpc
nr = 10000
r2 = 10.0
icr = 1.e-7   # Make sure this is small enough that solution has converged (do_check)
r1lin = r2 / nr
r1log = 1.e-3

# Potential parameters
kappa = np.sqrt(3./2.)
# kappa = 2.
lam = 15.
rd = 6.0  # in kpc
sigmab = 150.  # in km/s
mbh = 0.0  # in Msun

# Thermodynamic parameters.
# thot is chosen to be thot ~ tvir_DM, but not necessarily equal
rho_hot = 1.00  # in 1/cm3 (cgs)
thot = 3e6  # in K

# save figure to file?
do_file = False
do_plot = False
do_check = False
do_mass = False
do_effective_radius = True

# ______________________________________________________________________________________________________________________
# Main bit


# Interactive on
# pl.ion()

# Gas parameters
gamma = 5. / 3.
mu = 0.60364

# Relations for missing parameters
kappa2 = kappa * kappa
rho = lam ** 2 / kappa2
rb = rd / lam

# The DM and baryonic velocity dispersions (in cgs)
sigmab2 = sigmab * sigmab * 1.e10
sigmad2 = sigmab2 * kappa2
if do_check:
    print('sigma_d = ' + str(np.sqrt(sigmad2) * 1.e-5) + ' km/s')
    print('sigma_b = ' + str(np.sqrt(sigmab2) * 1.e-5) + ' km/s')

# DM and baryonic central densities (in particles per cm^3)
rhod0 = 9. * sigmad2 / (4. * np.pi * pc.newton * (rd * pc.kpc) ** 2) / (pc.amu * mu)
rhob0 = rho * rhod0
if do_check:
    print('rhod0 = ' + str(rhod0))
    print('rhob0 = ' + str(rhob0))

# Check requires both modes
if do_check:
    do_file = True
    do_plot = True

if do_mass:
    do_plot = True

# The radial domain, log and lin (in kpc)
rlin = np.linspace(r1lin, r2, nr)
rlin = np.insert(rlin, 0, icr)

rlog = np.logspace(np.log10(r1log), np.log10(r2), nr)
rlog = np.insert(rlog, 0, icr)

# Filename from parameters
fname = 'dip_k' + str(np.round(kappa, 2)) + '_l' + str(np.round(lam, 2)) + '_r' + str(np.round(rd, 2)) + '_t' + \
        format(thot / 1.e7, '.1f') + 'e7' + '_n' + format(rho_hot, '.1f')


# System of linear ODEs to integrate
# x[1] = g = dpsi/drp
# x[0] = psi

# Double isothermal
def fx(x, rp, rho=rho, kappa2=kappa2):
    return np.array([x[1], 9 * (np.exp(-x[0]) + rho * np.exp(-kappa2 * x[0])) - 2 * x[1] / rp])


# Jacobian matrix for double isothermal
def fx_jac(x, rp, rho=rho, kappa2=kappa2):
    return np.array([[0, 1], [-9 * (np.exp(-x[0]) + rho * kappa2 * np.exp(-kappa2 * x[0])), -2. / rp]])


# Single baryonic isothermal halo
def fx_b(x, rp, rho=rho, kappa2=kappa2):
    return np.array([x[1], 9 * rho * np.exp(-kappa2 * x[0]) - 2 * x[1] / rp])


# Jacobian matrix for single baryonic isothermal halo
def fx_jac_b(x, rp, rho=rho, kappa2=kappa2):
    return np.array([[0, 1], [-9 * rho * kappa2 * np.exp(-kappa2 * x[0]), -2. / rp]])


# Single DM isothermal halo
def fx_d(x, rp):
    return np.array([x[1], 9 * np.exp(-x[0]) - 2 * x[1] / rp])


# Jacobian matrix for single DM isothermal halo
def fx_jac_d(x, rp):
    return np.array([[0, 1], [-9 * np.exp(-x[0]), -2. / rp]])


def sersic(x, ie, re, ind):

    b = 2 * ind - 1. / 3.

    return ie * np.exp(-b * ((x / re) ** (1. / ind) - 1))


def jac_sersic(x, re, ind):
    return np.array([])

if do_effective_radius:

    # Integration of Poisson equation
    gr_small = 1.e-8
    ic = [0, gr_small]
    rp = rlin / rd
    y, infodict = inte.odeint(fx, ic, rp, Dfun=fx_jac, full_output=True)
    psi = y[:, 0]
    rho_dip_b = rhob0 * np.exp(-kappa2 * psi)

    # 1-D Spline representation
    spl = sin.splrep(rlin, rho_dip_b)

    # A 2D slab is sufficient to calculate projected density in spherical symmetry
    radii_2d = np.sqrt(rlin[:, None] * rlin[:, None] + rlin[None, :] * rlin[None, :])
    rho_dip_b_2d = sin.splev(radii_2d, spl)

    # Cutoff: Zero values beyond maximum radius
    rho_dip_b_2d = np.where(np.greater(radii_2d, rlin[-1]), 0, rho_dip_b_2d)

    # Project onto 1D
    rho_dip_b_proj = rho_dip_b_2d.sum(axis=1)
    prof_norm = rho_dip_b_proj[0]
    rho_dip_b_proj *= 1. / prof_norm

    # Fit sersic profile
    # params = sop.curve_fit(sersic, rlin[1:1000], rho_dip_b_proj[1:1000], (1., 1., 4.))
    # fit_params = params[0]
    # sersic_fit = sersic(rlin, *fit_params)

    # Cumulative light
    rho_dip_b_proj_cum = np.cumsum(rho_dip_b_proj)

    # Normalize
    light_norm = rho_dip_b_proj_cum[-1]
    rho_dip_b_proj_cum *= 1. / light_norm

    # Find effective radius and index
    re = rlin[np.greater(rho_dip_b_proj_cum, 0.5)][0]
    re_ind_lin = np.argwhere(np.greater(rho_dip_b_proj_cum, 0.5))[0, 0]

    # Some output of results
    print(f'\nEffective radius: {re} kpc')
    print(f'Effective radius / baryonic core radius: {re / (rd / lam):6.4f}')

    # Plot the enclosed mass profile
    f1 = pl.figure(figsize=(7, 5))
    pl.loglog(rlin, rho_dip_b_proj)
    pl.ylim(0.008, 1.05)
    pl.xlim(0.008, 5)
    # pl.loglog(rlin, sersic_fit)
    pl.ylabel('Surface brightness [ arbitrary units ]')
    pl.xlabel(r'$r$ [ kpc ]')
    pl.savefig(fname + '-light_profile.png', bbox_inches='tight')
    pl.savefig(fname + '-light_profile.pdf', bbox_inches='tight')



# Plot profiles in log space
if do_plot:

    # Scaled (to DM core radius) log radius used for integration.
    rp = rlog / rd

    # Initial conditions for double iso and baryons, and single DM
    # The gradient is very small near r = 0
    gr_small = 1.e-8
    ic = [0, gr_small]

    # Do the integration
    y, infodict = inte.odeint(fx, ic, rp, Dfun=fx_jac, full_output=True)
    y_b, infodict = inte.odeint(fx_b, ic, rp, Dfun=fx_jac_b, full_output=True)
    y_d, infodict = inte.odeint(fx_d, ic, rp, Dfun=fx_jac_d, full_output=True)

    # The potentials (in units of sigmad2)
    psid, psib, psi = y_d[:, 0], y_b[:, 0] * kappa2, y[:, 0]

    # Calculate and plot the density profiles of the underlying baryons and DM.
    rho_dip = rhod0 * np.exp(-psi) + rhob0 * np.exp(-kappa2 * psi)
    rho_dip_b = rhob0 * np.exp(-kappa2 * psi)
    rho_dip_d = rhod0 * np.exp(-psi)
    # rho_sg_d = rhod0 * np.exp(-psid)
    # rho_sg_b = rhob0 * np.exp(-psib)

    # Add Black hole potential to psi (in units of sigmad2)
    psi += -pc.newton * mbh * pc.msun / (rp * rd * pc.kpc) / sigmad2

    # DM: Do not assume Tvir = Thot. Calculate nhot consistently (in cgs)
    # If Tvir == Thot, we would have nhot = rho_hot * np.exp(-psi)
    tvir = sigmad2 * pc.amu * mu / pc.kboltz
    nhot = rho_hot * np.exp(-psi * tvir / thot)
    if do_check:
        print('tvir / thot = ' + str(tvir / thot))

    # Plot the potential profiles
    f1 = pl.figure(figsize=(7, 7))

    # Do plots
    ax1 = pl.subplot(211)
    ax1.plot(np.log10(rlog), np.log10(rho_dip), label='Double isothermal')
    ax1.plot(np.log10(rlog), np.log10(rho_dip_b), '--', label='Baryonic component')
    ax1.plot(np.log10(rlog), np.log10(rho_dip_d), '--', label='Dark Matter component')
    # ax1.plot(np.log10(rlog), np.log10(rho_sg_d), '-.', label='Single isothermal DM halo')
    # ax1.plot(np.log10(rlog), np.log10(rho_sg_b), '-.', label='Single isothermal baryonic halo')
    ax1.plot(np.log10(rlog), np.log10(nhot), '-', lw=1.4, label='Hot phase')
    # ax1.plot(np.log10(rlog), np.log10(rho_dip_b - nhot), '-', lw=1.4, label='Stellar component + warm/cold phase')
    ax1.axis([-3, 1, -3, 5])
    ax1.tick_params(labelbottom='off')
    pl.ylabel(r'$\log(\rho\,[\mathrm{cm}^{-3}])$')

    # Calc and plot the potential profiles
    ax2 = pl.subplot(212)
    ax2.plot(np.log10(rlog), (psi), label='Double isothermal')
    ax2.plot(np.log10(rlog), (psib), '-.', label='Single isothermal baryonic halo')
    ax2.plot(np.log10(rlog), (psid), '-.', label='Single isothermal DM halo')
    ax2.axis([-3, 1, -1.8, 9])
    pl.xlabel(r'$\log(r\,[\mathrm{kpc}])$')
    pl.ylabel(r'$\phi/\sigma_D^2$')

    pl.subplots_adjust(right=0.98, top=0.98, hspace=0.05)

    pl.sca(ax1)
    leg = pl.legend(loc=0, prop={'size': 10})
    ax1.add_artist(leg)

    pl.savefig(fname + '.png', bbox_inches='tight')
    pl.savefig(fname + '.pdf', bbox_inches='tight')

    if do_mass:

        # Find index of core radius
        four_pi_r2 = 4 * np.pi * rlog[1:] ** 2 * pc.kpc * pc.kpc * np.diff(rlog * pc.kpc)
        cummass_b = np.cumsum(four_pi_r2 * rho_dip_b[1:] * mu * pc.amu) / pc.msun
        cummass_d = np.cumsum(four_pi_r2 * rho_dip_d[1:] * mu * pc.amu) / pc.msun
        cummass = np.cumsum(four_pi_r2 * rho_dip[1:] * mu * pc.amu) / pc.msun

        # Plot the enclosed mass profile
        f1 = pl.figure(figsize=(7, 5))
        pl.loglog(rlog[1:], cummass, label='Total mass')
        pl.loglog(rlog[1:], cummass_d, '-.', label='DM mass')
        pl.loglog(rlog[1:], cummass_b, '--', label='Baryonic mass')
        pl.legend()
        # pl.grid(which='both')
        pl.savefig(fname + '-enclosed_mass.png', bbox_inches='tight')
        pl.savefig(fname + '-enclosed_mass.pdf', bbox_inches='tight')

# Save profile data to file. We use a linear grid in simulations
# so we you want to data in linsapce
if do_file:

    # Scaled radius
    rp = rlin / rd

    # Initial conditions for double iso and baryons, and single DM
    gr_small = 0.0
    ic = [0.0, gr_small]

    # Do the integration
    y, infodict = inte.odeint(fx, ic, rp, Dfun=fx_jac, full_output=True)
    y_b, infodict = inte.odeint(fx_b, ic, rp, Dfun=fx_jac_b, full_output=True)
    y_d, infodict = inte.odeint(fx_d, ic, rp, Dfun=fx_jac_d, full_output=True)

    # Potential and gradient of potential in units of sigmad2 and sigmad2 / rd
    psi, dpsi = y[:, 0], y[:, 1]

    # Turn psi into cgs units and add Black Hole potential.
    psi *= sigmad2
    psi += -pc.newton * mbh * pc.msun / (rlin * pc.kpc)

    # Turn dpsi int cgs and add Black Hole potential gradient (in cgs units)
    dpsi *= sigmad2 / (rd * pc.kpc)
    dpsi += pc.newton * mbh * pc.msun / (rlin * rlin * pc.kpc * pc.kpc)

    # DM: Do not assume Tvir = Thot. Calculate nhot consistently (in cgs, gr cm^-3)
    # rho_hot is in units of particles per unit volume.
    # If Tvir == Thot, we'd have nhot = rho_hot * np.exp(-psi)
    tvir = sigmad2 * pc.amu * mu / pc.kboltz
    nhot = rho_hot * mu * pc.amu * np.exp(-psi / sigmad2 * tvir / thot)

    # Data array to be saved (in cgs)
    pres = nhot * thot * pc.kboltz / (mu * pc.amu)
    sv_h = np.vstack((rlin * pc.kpc, nhot, pres)).T
    sv_g = np.vstack((rlin * pc.kpc, psi, dpsi)).T

    # File name and write to file. Number of lines after header, then the first
    # row, whose data has a largest significant digit at a decimal place smaller
    # than the width specified in fmt, i.e., colw. This, essentially gives the
    # boundary values at 0.
    fname_h = fname + '-h.dat'
    fname_g = fname + '-g.dat'
    colw, colp = '16', '8'
    np.savetxt(fname_h, sv_h, fmt='%-' + colw + '.' + colp + 'e', delimiter='')
    np.savetxt(fname_g, sv_g, fmt='%-' + colw + '.' + colp + 'e', delimiter='')


# Check hydrostatics (in cgs)
if do_check:
    dpdr = np.diff(pres) / np.diff(rlin * pc.kpc)
    ndpsi = nhot * dpsi
    ratio = -dpdr / (ndpsi[0:-1] + 0.5 * np.diff(ndpsi))
    print('hydrostatics - differences')
    print('max = ' + str(np.nanmax(np.abs(1 - ratio))))
    print('mean = ' + str(np.nanmean(np.abs(ratio))))
